describe("My First Test", function() {
    it("Visits the SfLearning technique list", function() {
        cy.visit("http://localhost:8080/web/techniques");

        cy.get(".technique").should("not.exist");

        cy.contains("Add").click();

        cy.url().should("include", "/techniques/create");

        cy.get("input")
            .type("Technique cypress")
            .should("have.value", "Technique cypress");

        cy.get("textarea")
            .type("First cypress test")
            .should("have.value", "First cypress test");

        cy.contains("Create").click();

        cy.get(".technique").contains("Technique cypress");
        cy.get(".technique").contains("First cypress test");

        cy.get(".technique")
            .contains("Edit")
            .click();

        cy.get("input")
            .type(" 2")
            .should("have.value", "Technique cypress 2");

        cy.get("textarea").should("have.value", "First cypress test");

        cy.contains("Update").click();

        cy.contains("Technique cypress 2 updated");
        cy.get(".technique").contains("Technique cypress 2");
        cy.get(".technique").contains("First cypress test");

        cy.contains("Delete").click();

        cy.contains("No").click();

        cy.contains("Technique cypress 2");
        cy.contains("First cypress test");

        cy.contains("Delete").click();

        cy.contains("Yes").click();
        cy.contains("Technique cypress 2 deleted");
        cy.get(".technique").should("not.exist");
    });
});
