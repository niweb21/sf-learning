<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Technique;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * @Route("/techniques")
 */
class TechniqueController extends Controller
{
    /**
     * @Route("/", name="technique_list")
     * @Method({"GET"})
     */
    public function listAction()
    {
        $techniqueRepository = $this->get('technique_repository');
        $techniques = $techniqueRepository->findAll();

        return $this->render('AppBundle:Technique:list.html.twig', [
            'techniques' => $techniques,
        ]);
    }

    /**
     * @Route("/create", name="technique_create_form")
     * @Method({"GET", "POST"})
     */
    public function createFormAction(Request $request)
    {
        $technique = new Technique();

        $form = $this->createFormBuilder($technique)
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('create', SubmitType::class, ['label' => 'Create'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $technique = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($technique);
            $entityManager->flush();

            $this->addFlash('notice', $technique->getName().' added');

            return $this->redirectToRoute('technique_list');
        }

        return $this->render('AppBundle:Technique:create_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="technique_detail")
     * @Method({"GET"})
     */
    public function detailAction($id)
    {
        $techniqueRepository = $this->get('technique_repository');
        $technique = $techniqueRepository->findOneBy(['id' => $id]);

        if (is_null($technique)) {
            throw $this->createNotFoundException();
        }

        return $this->render('AppBundle:Technique:detail.html.twig', [
            'technique' => $technique,
        ]);
    }

    /**
     * @Route("/update/{id}", name="technique_update_form")
     * @Method({"GET"})
     */
    public function updateFormAction($id)
    {
        $techniqueRepository = $this->get('technique_repository');
        $technique = $techniqueRepository->findOneBy(['id' => $id]);

        if (is_null($technique)) {
            throw $this->createNotFoundException();
        }

        return $this->render('AppBundle:Technique:update_form.html.twig', [
            'technique' => $technique,
        ]);
    }

    /**
     * @Route("/{id}", name="technique_update")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $id)
    {
        $techniqueRepository = $this->get('technique_repository');
        $technique = $techniqueRepository->findOneBy(['id' => $id]);

        if (is_null($technique)) {
            throw $this->createNotFoundException();
        }

        $technique->setName($request->request->get('name'));
        $technique->setDescription($request->request->get('description'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($technique);
        $em->flush();

        $this->addFlash('notice', $technique->getName().' updated');

        return $this->redirectToRoute('technique_list');
    }

    /**
     * @Route("/delete/{id}", name="technique_delete_form")
     * @Method({"GET"})
     */
    public function deleteFormAction($id)
    {
        $techniqueRepository = $this->get('technique_repository');
        $technique = $techniqueRepository->findOneBy(['id' => $id]);

        if (is_null($technique)) {
            throw $this->createNotFoundException();
        }

        return $this->render('AppBundle:Technique:delete_form.html.twig', [
            'technique' => $technique,
        ]);
    }

    /**
     * @Route("/{id}/delete", name="technique_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $techniqueRepository = $this->get('technique_repository');
        $technique = $techniqueRepository->findOneBy(['id' => $id]);

        if (is_null($technique)) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($technique);
        $em->flush();

        $this->addFlash('notice', $technique->getName().' deleted');

        return $this->redirectToRoute('technique_list');
    }
}
