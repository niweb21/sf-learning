<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TechniqueControllerTest extends WebTestCase
{
    public function testCrud()
    {
        $this->listHasNoTechnique();
        $this->addTechnique();
        $this->editTechnique();
        $this->deleteTechnique();
    }

    private function listHasNoTechnique()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/techniques/');

        $this->assertEquals(
            0,
            $crawler->filter('html:contains("Fonctionnal Name 1")')->count()
        );

        $link = $crawler->selectLink('Add')->link();
        $this->assertContains('/techniques/create', $link->getUri());
    }

    private function addTechnique()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/techniques/create');

        $form = $crawler->selectButton('Create')->form([
            'name' => 'Fonctionnal Name 1',
            'description' => 'Fonctionnal Description 1!',
        ]);

        $crawlerListRedirect = $client->submit($form);
        $this->assertGreaterThan(
            0,
            $crawlerListRedirect->filter('html:contains("Redirecting to /techniques")')->count()
        );

        $crawler = $client->request('GET', '/techniques/');
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 1 added")')->count()
        );
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 1")')->count()
        );
    }

    private function editTechnique()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/techniques/');

        $link = $crawler->selectLink('Edit')->last()->link();

        $crawler = $client->click($link);

        $form = $crawler->selectButton('Update')->form([
            'name' => 'Fonctionnal Name 2',
            'description' => 'Fonctionnal Description 2!',
        ]);

        $crawlerListRedirect = $client->submit($form);
        $this->assertGreaterThan(
            0,
            $crawlerListRedirect->filter('html:contains("Redirecting to /techniques")')->count()
        );

        $crawler = $client->request('GET', '/techniques/');
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 2 updated")')->count()
        );
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 2")')->count()
        );
    }

    private function deleteTechnique()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/techniques/');

        $crawler = $client->click($crawler->selectLink('Delete')->last()->link());
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 2")')->count()
        );

        $crawler = $client->click($crawler->selectLink('Yes')->link());
        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("Redirecting to /techniques")')->count()
        );

        $crawler = $client->request('GET', '/techniques/');
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 2 deleted")')->count()
        );
        $this->assertEquals(
            1,
            $crawler->filter('html:contains("Fonctionnal Name 2")')->count()
        );
    }
}
