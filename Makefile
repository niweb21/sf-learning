up:
	docker-compose up -d

install_dep:
	docker-compose exec web sh -c 'composer install'

init_database:
	docker-compose exec web php app/console doctrine:schema:create

bash:
	docker-compose exec web /bin/bash

console:
	docker-compose exec web php app/console ${ARGS}

mysql:
	docker-compose exec db sh -c 'exec mysql -uroot -p$$MYSQL_ROOT_PASSWORD $$MYSQL_DATABASE'

test:
	docker-compose exec web ./bin/simple-phpunit -c app ${ARGS}

ugli_ci_cypress:
	docker-compose down
	docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d
	sleep 5
	$(MAKE) console ARGS="c:c -e prod"
	$(MAKE) console ARGS="doctrine:schema:create"

	docker-compose -f docker-compose.yml -f docker-compose.test.yml -f docker-compose.cypress.yml run cypress
	docker-compose down

cypress_local_install:
	cd cypress && npm install

cypress_local_open: cypress_local_install
	cd cypress && $$(npm bin)/cypress open
